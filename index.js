const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();
const employees = require("./api/employees");
const categoryColors = require("./api/category_colors");
const members = require("./api/members");
const drivers = require("./api/drivers");
const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost:27017/myapp", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use("/employees", employees);
app.use("/category_colors", categoryColors);
app.use("/members", members);
app.use("/drivers", drivers);

app.get("/", (req, res) => {
  res.send("Hai Guys");
});

app.listen(3000, () => console.log(`Example app listening on port 3000! `));
