const Drivers = require("./model");

const getAll = (req, res) => {
  Drivers.find({}).exec((err, drivers) => {
    res.send({
      success: true,
      drivers: drivers,
    });
  });
};

const createOne = (req, res) => {
  const newDriver = new Drivers(req.body);

  newDriver.save((err, newData) => {
    if (err)
      return res.send({
        success: false,
        message: err.message,
      });

    res.send({
      new_driver: newData,
      success: true,
    });
  });
};

const getById = (req, res) => {
  Drivers.findOne({ _id: req.params.id }, (err, driver) => {
    res.send({
      success: true,
      driver: driver,
    });
  });
};

const deleteAll = (req, res) => {
  CategoryColors.remove({}, (error) => {
    if (error) res.status(400).json({ error: error });
    res.status(200).send({ message: "All colors have been removed." });
  });
};

const deleteById = (req, res) => {
  // Remove one resource by id
  Drivers.remove({ _id: req.params.id }, (error, driver) => {
    res.send({
      success: true,
      message: `Member with id: ${req.params.id} has been deleted`,
      data: driver,
    });
  });
};

const editById = (req, res) => {
  // Create new resource object data
  const newDriver = req.body;

  Drivers.findByIdAndUpdate(
    { _id: req.params.id },
    newDriver,
    { new: true },
    function (err, data) {
      if (err) {
        res.send({
          success: false,
          message: err.message,
        });
      } else {
        res.send({
          success: true,
          data: data,
        });
      }
    }
  );
};

module.exports = {
  getAll,
  createOne,
  getById,
  deleteAll,
  deleteById,
  editById,
};
