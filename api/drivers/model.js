const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const modelName = "drivers";
const schema = new Schema(
  {
    name: {
      type: String,
    },
    birth_date: {
      type: Date,
    },
    birth_place: {
      type: String,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model(modelName, schema);
