const Employee = require("./model");
const nodemailer = require("nodemailer");

const getAll = (req, res) => {
  Employee.find({})
    .populate({
      path: "posts",
    })
    .exec((err, accounts) => {
      res.send({
        data: accounts,
      });
    });
};

const createOne = (req, res) => {
  const newEmployee = new Employee(req.body);

  newEmployee.save((err) => {
    if (err) res.send("error");
    else {
      res.send({
        registered: newEmployee,
        success: true,
      });
    }
  });
};

const getById = (req, res) => {
  Employee.findOne({ id: Number(req.params.id) }, (err, account) => {
    res.send({
      params: req.params,
      data: account,
    });
  });
};

const deleteAll = (req, res) => {
  Employee.remove({}, (error) => {
    if (error) res.status(400).json({ error: error });
    res.status(200).send({ message: "All accounts have been removed." });
  });
};

const deleteById = (req, res) => {
  // Remove one resource by id
  Employee.findByIdAndRemove(req.params.id, (error, employees) => {
    if (employees !== null) {
      res.send({
        message: `Post with id: ${req.params.id} has been deleted`,
        data: employees,
      });
    } else {
      res.send({
        message: `Data not found`,
      });
    }
  });
};

const editById = (req, res) => {
  // Create new resource object data
  const newEmployee = req.body;

  Employee.findByIdAndUpdate({ _id: req.params.id }, newEmployee, function (
    err,
    result
  ) {
    if (err) {
      res.send(err);
    } else {
      res.send(result);
    }
  });
};

const login = (req, res) => {
  const body = req.body;
  Employee.findOne({ email: body.email, password: body.password }, function (
    err,
    employee
  ) {
    if (err !== null) {
      return res.send({
        success: false,
        message: "Error",
      });
    }

    if (employee == null) {
      return res.send({
        success: false,
        message: "Account not found or wrong password",
      });
    }

    res.send({
      success: true,
    });
  });
};

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "beyondblindnessadelaide@gmail.com",
    pass: "adelaide2020",
  },
});

const resetPassword = (req, res) => {
  const body = req.body;

  Employee.findOne({ email: body.email }, (err, account) => {
    if (err || !account) {
      return res.send({
        success: false,
      });
    }

    const mailOptions = {
      from: "beyondblindnessadelaide@gmail.com", // sender address
      to: body.email, // list of receivers
      subject: "Reset Your Password", // Subject line
      html: `http://localhost:5500/change_password.html?email=${body.email}`, // plain text body
    };

    transporter.sendMail(mailOptions, function (err, info) {
      if (err) console.log(err);
      else console.log(info);
    });

    res.send({
      success: true,
      data: account,
    });
  });
};

const changePassword = (req, res) => {
  const body = req.body;

  Employee.findOneAndUpdate(
    { email: body.email },
    { password: body.password },
    { new: true },
    (err, employee) => {
      if (err || !employee) {
        res.send({
          success: false,
        });
      }

      res.send({
        success: true,
        data: employee,
      });
    }
  );
};

module.exports = {
  getAll,
  createOne,
  getById,
  deleteAll,
  deleteById,
  editById,
  login,
  resetPassword,
  changePassword,
};
