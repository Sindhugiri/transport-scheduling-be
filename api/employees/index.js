const express = require("express");
const router = express.Router();
const controller = require("./controller");

router.get("/", controller.getAll);
router.get("/:id", controller.getById);
router.post("/reset_password", controller.resetPassword);
router.post("/login", controller.login);
router.post("/", controller.createOne);
router.put("/change_password", controller.changePassword);
router.put("/:id", controller.editById);
router.delete("/:id", controller.deleteById);
router.delete("/", controller.deleteAll);

module.exports = router;
