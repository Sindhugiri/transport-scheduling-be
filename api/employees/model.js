const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const modelName = "employee";
const schema = new Schema(
  {
    name: {
      type: String,
    },
    email: {
      type: String,
      unique: true,
      lowercase: true,
    },
    password: String,
    birth_date: Date,
    birth_place: String,
    job_title: String,
  },
  { timestamps: true }
);

module.exports = mongoose.model(modelName, schema);
