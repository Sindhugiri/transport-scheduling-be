const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const modelName = "category_colors";
const schema = new Schema(
  {
    category: {
      type: String,
      unique: true,
    },
    type: {
      enum: ["vehicle", "appointment"],
      type: String,
    },
    hex_color: {
      type: String,
      unique: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model(modelName, schema);

/* 
[
  "bus",
  "car",
  "weekly",
  "fortnightly",
  "yearly",
  "volunteer_owned",
]
*/
