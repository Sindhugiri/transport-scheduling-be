const CategoryColors = require("./model");

const getAll = (req, res) => {
  CategoryColors.find({})
    .populate({
      path: "posts",
    })
    .exec((err, colors) => {
      res.send({
        colors: colors,
      });
    });
};

const createOne = (req, res) => {
  const newColor = new CategoryColors(req.body);

  newColor.save((err, newData) => {
    if (err)
      return res.send({
        success: false,
        message: err.message,
      });

    res.send({
      new_color: newData,
      success: true,
    });
  });
};

const getById = (req, res) => {
  CategoryColors.findOne({ _id: req.params.id }, (err, color) => {
    res.send({
      success: true,
      color: color,
    });
  });
};

const deleteAll = (req, res) => {
  CategoryColors.remove({}, (error) => {
    if (error) res.status(400).json({ error: error });
    res.status(200).send({ message: "All colors have been removed." });
  });
};

const deleteById = (req, res) => {
  // Remove one resource by id
  CategoryColors.remove({ _id: req.params.id }, (error, color) => {
    res.send({
      success: true,
      message: `Color with id: ${req.params.id} has been deleted`,
      data: color,
    });
  });
};

const editById = (req, res) => {
  // Create new resource object data
  const newEmployee = req.body;

  CategoryColors.findByIdAndUpdate(
    { _id: req.params.id },
    newEmployee,
    { new: true },
    function (err, data) {
      if (err) {
        res.send({
          success: false,
          message: err.message,
        });
      } else {
        res.send({
          success: true,
          data: data,
        });
      }
    }
  );
};

module.exports = {
  getAll,
  createOne,
  getById,
  deleteAll,
  deleteById,
  editById,
};
